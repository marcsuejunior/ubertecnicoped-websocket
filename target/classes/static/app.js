var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        var subscription = stompClient.subscribe('/topic/greetings', function (greeting) {
        
            /*showGreeting(JSON.parse(greeting.body).content);*/
            /*var response=(JSON.parse(greeting.body).);*/
            console.log(JSON.parse(greeting.body));
            
            /*console.log(response);*/
            
        });

        console.log(subscription);
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();

    }
    setConnected(false);
    stompClient = null;
    console.log("Disconnected");
    
}

function sendName() {
    stompClient.send("/app/localizacao", {}, JSON.stringify({'latitude':18.0,'longitude':40.0}));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});