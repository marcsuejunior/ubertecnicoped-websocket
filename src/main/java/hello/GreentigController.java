package hello;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;



@Controller
public class GreentigController 
{	
	@SuppressWarnings("unused")
	@MessageMapping("/localizacao")
	 	@SendTo("/topic/greetings")
	 	public PositionModel posicaoTecnico(PositionModel position) throws Exception 
	 	{
		 	Thread.sleep(1000);	 	
			
		 	System.out.println("lat: "+position.getLatitude()+" longitude: "+position.getLongitude());
			
			if(position!=null)
				return position;
			else
				return null;
	 	}
	

}
